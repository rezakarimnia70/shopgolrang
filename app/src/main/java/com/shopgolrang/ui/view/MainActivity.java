package com.shopgolrang.ui.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.shopgolrang.R;
import com.shopgolrang.utils.SwitchFragments;

public class MainActivity extends AppCompatActivity implements SwitchFragments {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startFragment(CategoryAndProductFragment.newInstance());
        findViewById(R.id.btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(findViewById(R.id.lin1 ), "sdcsdcdsc" , Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onSwitch(Fragment newFragment) {
        startFragment(newFragment);
    }

    public void startFragment(Fragment newFragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, newFragment);
        ft.addToBackStack(newFragment.getClass().getSimpleName());
        ft.commit();
    }
}