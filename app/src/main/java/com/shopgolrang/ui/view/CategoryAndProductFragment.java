package com.shopgolrang.ui.view;

import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import com.shopgolrang.R;
import com.shopgolrang.base.mvvm.BaseMVVMFragment;
import com.shopgolrang.databinding.FragmentCategoryAndProductBinding;
import com.shopgolrang.ui.adapter.AdapterCategory;
import com.shopgolrang.ui.adapter.AdapterProduct;
import com.shopgolrang.ui.viewmodel.CategoryAndProductViewModels;
import com.shopgolrang.utils.SwitchFragments;

public class CategoryAndProductFragment extends BaseMVVMFragment<CategoryAndProductViewModels> {

    private FragmentCategoryAndProductBinding mBinding;
    private AdapterCategory adapterCategory;
    private AdapterProduct adapterProduct;

    public static CategoryAndProductFragment newInstance() {
        return new CategoryAndProductFragment();
    }

    @Override
    protected CategoryAndProductViewModels getViewModel() {
        return new ViewModelProvider(this).get(CategoryAndProductViewModels.class);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_category_and_product;
    }

    @Override
    protected ViewBinding getBindView() {
        mBinding = FragmentCategoryAndProductBinding.inflate(getLayoutInflater());
        return mBinding;
    }

    @Override
    protected void setupView() {
        mBinding.layoutCategory.progressCategory.setVisibility(View.VISIBLE);
        mBinding.layoutProduct.progressProduct.setVisibility(View.VISIBLE);
        mViewModel.showListCategory();
        observerListCategory();
        mViewModel.showListProduct();
        observerListProduct();
    }

    private void observerListCategory()
    {
        setAdapterCategory();
        mViewModel.getCategoryLiveData().observe(this, category -> {
            adapterCategory.setCategoryDataList(mContext, category);
            adapterCategory.notifyDataSetChanged();
            mBinding.layoutCategory.progressCategory.setVisibility(View.GONE);
        });
    }

    private void setAdapterCategory() {
        adapterCategory = new AdapterCategory();
        mBinding.layoutCategory.recyclerCategories.setLayoutManager(new
                LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mBinding.layoutCategory.recyclerCategories.setAdapter(adapterCategory);
    }

    private void observerListProduct() {
        setAdapterProduct();
        mViewModel.getProductLiveData().observe(this, products -> {
            adapterProduct.setProductDataList(products);
            adapterProduct.notifyDataSetChanged();
            mBinding.layoutProduct.progressProduct.setVisibility(View.GONE);

        });
    }

    private void setAdapterProduct() {
        adapterProduct = new AdapterProduct((position, products) -> {
            if (mContext instanceof SwitchFragments)
                ((SwitchFragments) mContext).onSwitch(ProductFragment.newInstance(products.get(position).getId()));
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2, RecyclerView.VERTICAL, false);
        mBinding.layoutProduct.recyclerProduct.setLayoutManager(gridLayoutManager);
        mBinding.layoutProduct.recyclerProduct.setAdapter(adapterProduct);
    }
}