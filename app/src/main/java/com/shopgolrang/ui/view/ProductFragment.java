package com.shopgolrang.ui.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import com.bumptech.glide.load.data.ExifOrientationStream;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.shopgolrang.R;
import com.shopgolrang.base.BaseFragment;
import com.shopgolrang.base.mvvm.BaseMVVMFragment;
import com.shopgolrang.data.model.Product;
import com.shopgolrang.databinding.FragmentProductBinding;
import com.shopgolrang.ui.viewmodel.ProductViewModel;

import java.util.ArrayList;
import java.util.List;

public class ProductFragment extends BaseMVVMFragment<ProductViewModel> implements View.OnClickListener {

    private FragmentProductBinding mBinding;
    private int idProduct, productCount;

    public static ProductFragment newInstance(int idProduct) {
        Bundle args = new Bundle();
        args.putInt("idProduct", idProduct);
        ProductFragment productFragment = new ProductFragment();
        productFragment.setArguments(args);
        return productFragment;
    }

    @Override
    protected ProductViewModel getViewModel() {
        return new ViewModelProvider(this).get(ProductViewModel.class);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_product;
    }

    @Override
    protected ViewBinding getBindView() {
        mBinding = FragmentProductBinding.inflate(getLayoutInflater());
        return mBinding;
    }

    @Override
    protected void setupView() {
        setBundle();
        showProduct();
        mBinding.imgAdd.setOnClickListener(this);
        mBinding.imgMinus.setOnClickListener(this);
        mBinding.addProductCard.setOnClickListener(this);

    }

    private void setBundle() {
        if (getArguments() != null)
            idProduct = getArguments().getInt("idProduct");
    }

    private void observeProduct() {
        mViewModel.getProductDataLiveData().observe(this, new Observer<Product>() {
            @Override
            public void onChanged(Product product) {
                mBinding.setProductItem(product);
                mBinding.txtDetailsProduct.setMovementMethod(new ScrollingMovementMethod());
            }
        });
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_add:
                addCountProduct();
                break;
            case R.id.img_minus:
                minusCountProduct();
                break;
            case R.id.add_product_card:
                addProductCard();
                break;
        }
    }

    /*private void addProductCard() {
        if (productCount == 0)
            Toast.makeText(mContext, "pleas enter count product", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, mContext.getString(R.string.msg_success_add_card), Toast.LENGTH_SHORT).show();
    }
*/
    private void minusCountProduct() {
        if (productCount > 0) {
            productCount--;
            mBinding.txtCount.setText(String.valueOf(productCount));
        } else
            mBinding.txtCount.setText("0");
    }

    private void addCountProduct() {
        productCount++;
        mBinding.txtCount.setText(String.valueOf(productCount));
    }

    private void showProduct() {
        if (idProduct != 0) {
            mViewModel.showProduct(idProduct);
            observeProduct();
        } else
            Toast.makeText(mContext, "Not ID", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("ShowToast")
    private void addProductCard() {
        /*Snackbar snackbar = Snackbar.make(mBinding.constProduct, mContext.getString(R.string.msg_success_add_card)
                , Snackbar.LENGTH_SHORT);

        View backgroundSnackbar = snackbar.getView();
        backgroundSnackbar.setBackgroundResource(R.color.colorGreen);
        TextView snackTextview = backgroundSnackbar.findViewById(com.google.android.material.R.id.snackbar_text);
        snackTextview.setTextColor(getResources().getColor(R.color.white));
        snackbar.show();*/
    }
}
