package com.shopgolrang.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.shopgolrang.data.model.Product;
import com.shopgolrang.databinding.ItemProductBinding;
import com.shopgolrang.ui.OnClickListener;
import com.shopgolrang.ui.view.ProductFragment;
import com.shopgolrang.utils.SwitchFragments;

import java.util.ArrayList;
import java.util.List;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.MyViewHolder> {
    private List<Product> productList = new ArrayList<>();
    private OnClickListener onClickListener;

    public void setProductDataList(List<Product> productList) {
        this.productList = productList;
    }

    public AdapterProduct(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemProductBinding binding = ItemProductBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyViewHolder(binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull AdapterProduct.MyViewHolder holder, int position) {
        holder.itemBinding.setItem(productList.get(position));
        holder.itemView.setOnClickListener(v -> onClickListener.onCLick(position, productList));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    private void setImageProduct(Context context, ImageView img, int position) {
        Glide.with(context)
                .load(productList.get(position).getImage())
                .override(130, 150)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(img);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ItemProductBinding itemBinding;

        public MyViewHolder(@NonNull ItemProductBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }


}
