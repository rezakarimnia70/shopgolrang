package com.shopgolrang.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shopgolrang.R;
import com.shopgolrang.databinding.ItemCatgoriesBinding;

import java.util.ArrayList;
import java.util.List;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.MyViewHolder> {
    private List<String> categoryDataList = new ArrayList<>();
    private Context context;
    private final List<Boolean> listSelect = new ArrayList<>();

    public void setCategoryDataList(Context context, List<String> categoryDataList) {
        this.context = context;
        this.categoryDataList = categoryDataList;
        for (int i = 0; i < categoryDataList.size(); i++) {
            listSelect.add(false);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCatgoriesBinding binding = ItemCatgoriesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCategory.MyViewHolder holder, int position) {
        holder.itemBinding.btnCategory.setText(categoryDataList.get(position));
        holder.itemBinding.btnCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCheckSelectItem(position);
                holder.itemBinding.btnCategory.setTextColor(context.getResources().getColor(R.color.colorOrange));
            }
        });
        setCheckItem(position, holder.itemBinding.btnCategory);

    }

    @Override
    public int getItemCount() {
        return categoryDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ItemCatgoriesBinding itemBinding;

        public MyViewHolder(@NonNull ItemCatgoriesBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }

    private void setCheckSelectItem(int position) {
        for (int i = 0; i < listSelect.size(); i++) {
            if (i == position)
                listSelect.set(position, true);
            else
                listSelect.set(i, false);
        }
        notifyDataSetChanged();
    }

    private void setCheckItem(int position, Button btnCategory) {
        if (listSelect.get(position)) {
            btnCategory.setTextColor(context.getResources().getColor(R.color.colorOrange));
            btnCategory.setBackgroundResource(R.drawable.rec_orange_btn_categories);

        } else {
            btnCategory.setTextColor(context.getResources().getColor(R.color.colorGrayText));
            btnCategory.setBackgroundResource(R.drawable.rec_gray_btn_categories);
        }
    }
}
