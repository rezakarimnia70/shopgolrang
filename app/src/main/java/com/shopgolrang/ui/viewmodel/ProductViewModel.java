package com.shopgolrang.ui.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.shopgolrang.base.mvvm.BaseViewModel;
import com.shopgolrang.data.model.Product;
import com.shopgolrang.data.repository.CategoryAndProductRepository;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ProductViewModel extends BaseViewModel<CategoryAndProductRepository> {

    private final MutableLiveData<Product> productDataLiveData = new MutableLiveData<>();

    public ProductViewModel(Application application) {
        super(application);
        setRepository(new CategoryAndProductRepository());
    }

    public void showProduct(int productId){
        Disposable disposable ;
        disposable = getProduct(productId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::setProductDataLiveData,
                        this::logError
                );

        addDisposable(disposable);
    }
    public MutableLiveData<Product> getProductDataLiveData() {
        return productDataLiveData;
    }

    public void setProductDataLiveData(Product productData) {
        productDataLiveData.setValue(productData);
    }

    private Flowable<Product> getProduct(int productId){
        return getRepository().getProduct(productId);
    }
}
