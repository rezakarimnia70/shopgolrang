package com.shopgolrang.data.remote;


import com.shopgolrang.data.model.Product;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("products/categories")
    Flowable<List<String>> getListCategory();

    @GET("products")
    Flowable<List<Product>> getListProduct();

    @GET("products/{productId}")
    Flowable<Product> getProduct(@Path("productId")int productId);
}