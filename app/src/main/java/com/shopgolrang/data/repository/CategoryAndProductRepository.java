package com.shopgolrang.data.repository;

import com.shopgolrang.data.model.Product;
import com.shopgolrang.data.remote.APIInterface;
import com.shopgolrang.data.remote.ApiClient;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;

public class CategoryAndProductRepository implements CategoryAndProductRepositoryHandler {

    @Override
    public Flowable<List<String>> getListCategory() {
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        return apiInterface.getListCategory();
    }

    @Override
    public Flowable<List<Product>> getListProduct() {
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        return apiInterface.getListProduct();
    }

    @Override
    public Flowable<Product> getProduct(int productId) {
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        return apiInterface.getProduct(productId);
    }
}
