package com.shopgolrang.data.repository;

import com.shopgolrang.data.model.Product;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;

public interface CategoryAndProductRepositoryHandler {

    Flowable<List<String>> getListCategory();

    Flowable<List<Product>> getListProduct();

    Flowable<Product> getProduct(int productId);
}
