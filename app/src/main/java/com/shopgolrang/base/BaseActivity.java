package com.shopgolrang.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewbinding.ViewBinding;

public abstract class BaseActivity extends AppCompatActivity {
    protected View currView;
    protected String currLayoutName;
    private boolean isSendData = false;

    private void manageFullScreen(AppCompatActivity activity) {
        /*if (!packageNameTheme.equals(SyncStateContract.Constants.DEFAULT_THEME) &&
                ((new UserDataUtil()).getVersionCodeOfAnotherApp(activity, packageNameTheme) ==
                        PreviewThemesActivity.VERSION_CODE_THEME_COMPATIBILITY)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = activity.getWindow();
                // clear FLAG_TRANSLUCENT_STATUS flag:
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                // finally change the color

                window.setStatusBarColor(this.getResources().getColor(R.color.design_default_color_primary));
            }
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manageFullScreen(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    /**
     * using of this method for Activity with viewBounding
     *
     * @param layoutId  : id layout on resource , start with R.layout.activity_id (it is not necessary for binding and just us on theme)
     * @param binding   : an instance of binding class, generated for corresponding layout
     * @param viewName: a string that describe class functionality
     */
    public void setLayoutView(ViewBinding binding, int layoutId, String viewName) {
        sendFireBaseLog(viewName);
        setLayoutView(binding, layoutId);
    }

    private void setLayoutView(ViewBinding binding, int layoutId) {
        currView = binding.getRoot();
        currLayoutName = getResources().getResourceEntryName(layoutId);
        setContentView(currView);
      //  UtilTheme.getInstance().setTheme(currView, currLayoutName);
    }

    /**
     * using of this method for Activity without viewBounding
     *
     * @param layoutId  : id layout on resource , start with R.layout.activity_id
     * @param viewName: a string that describe class functionality
     */
    public void setLayoutView(int layoutId, String viewName) {
        sendFireBaseLog(viewName);
        setLayoutView(layoutId);
    }

    private void setLayoutView(int layoutId) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        currView = inflater.inflate(layoutId, null);
        currLayoutName = getResources().getResourceEntryName(layoutId);
        setContentView(currView);
       // UtilTheme.getInstance().setTheme(currView, currLayoutName);
    }

    public void sendFireBaseLog(String viewName) {
        if (!isSendData) {
         //   FireBaseLog.sendActivityPage(viewName);
            isSendData = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void navigate(Fragment fragment, int containerId, boolean addToBackStack, String tag, boolean isAnimation) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (isAnimation) {
       //     fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        }
        fragmentTransaction.add(containerId, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack("");
        }
        fragmentTransaction.commit();
    }

    protected void navigateWithCustomAnimation(Fragment fragment, int containerId, boolean addToBackStack, String tag, int enterAnimation,
            int exitAnimation, int popEnterAnimation, int popExitAnimation) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation);
        fragmentTransaction.add(containerId, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack("");
        }
        fragmentTransaction.commit();
    }
}
