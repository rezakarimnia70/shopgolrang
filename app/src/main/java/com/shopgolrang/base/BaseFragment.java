package com.shopgolrang.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewbinding.ViewBinding;


public abstract class BaseFragment extends Fragment {
    protected View currView;
    protected Context mContext;
    protected boolean isActive;
    private String currLayoutName;
    private String mClassName = null;
    private ViewBinding binding;


    /**
     * using of this method for fragment with viewBounding
     *
     * @param layoutId    : id layout on resource , start with R.layout.fragment_id
     * @param viewBinding : an instance of binding class, generated for corresponding layout
     * @param className:  a string that describe class functionality
     */
    public void setLayoutView(ViewBinding viewBinding, int layoutId, String className) {
        setLayoutView(viewBinding, layoutId);
        mClassName = className;
    }

    private void setLayoutView(ViewBinding viewBinding, int layoutId) {
        binding = viewBinding;
        currView = binding.getRoot();
        currLayoutName = getResources().getResourceEntryName(layoutId);
        mClassName = currLayoutName;
        mContext = getContext();
    }

    /**
     * using of this method for fragment without viewBounding
     *
     * @param layoutId   : id layout on resource , start with R.layout.fragment_id
     * @param inflater   : LayoutInflater object from onCreateView
     * @param container: viewGroup object from onCreateView
     * @param className: a string that describe class functionality
     */
    public void setLayoutView(int layoutId, LayoutInflater inflater, ViewGroup container,
            String className) {
        setLayoutView(layoutId, inflater, container);
        mClassName = className;
    }

    /**
     * this method should be private and stop of using this
     */
    public void setLayoutView(int layoutId, LayoutInflater inflater, ViewGroup container) {
        currView = inflater.inflate(layoutId, container, false);
        currLayoutName = getResources().getResourceEntryName(layoutId);
        mClassName = currLayoutName;
        mContext = getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        isActive = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        isActive = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
