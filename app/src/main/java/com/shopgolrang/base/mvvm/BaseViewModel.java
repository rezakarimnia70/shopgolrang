package com.shopgolrang.base.mvvm;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;


public abstract class BaseViewModel<R> extends AndroidViewModel implements LifecycleObserver {

    private R repository;
    private final CompositeDisposable mCompositeDisposable;
    private Lifecycle mLifecycle;

    public BaseViewModel(Application application) {
        super(application);
        this.mCompositeDisposable = new CompositeDisposable();
    }

    public void setLifeCycle(Lifecycle lifecycle) {
        mLifecycle = lifecycle;
        addLifecycle(lifecycle);
    }

    private void addLifecycle(Lifecycle lifecycle) {
        lifecycle.addObserver(this);
    }

    protected void setRepository(R repository) {
        this.repository = repository;
    }

    protected R getRepository() {
        return repository;
    }


    @Override
    protected void onCleared() {
        mCompositeDisposable.clear();
        repository = null;
        if (mLifecycle != null) {
            mLifecycle.removeObserver(this);
        }
        super.onCleared();
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public void addDisposable(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }
/*
    protected boolean isNetworkConnected() {
        return ConnectionUtil.isConnection(getApplication());
    }*/

    protected String getString(int id) {
        return getApplication().getString(id);
    }

    public Lifecycle getLifecycle() {
        return mLifecycle;
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    protected void onStartView() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected void onResumeView() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected void onDestroy() {
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    protected void onPauseView() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    protected void onStopView() {
    }

    public void logError(Throwable onError) {
        if (onError != null && onError.getMessage() != null)
            Log.i("onError", "onErrorProduct   " + onError.getMessage());
    }
}
