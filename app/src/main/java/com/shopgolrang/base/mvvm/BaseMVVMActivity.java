package com.shopgolrang.base.mvvm;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;

import com.shopgolrang.base.BaseActivity;


public abstract class BaseMVVMActivity<V extends BaseViewModel<?>> extends BaseActivity {

    private static final int NO_UI_ACTIVITY = 0;

    protected V mViewModel;

    protected abstract V getViewModel();

    /**
     * @return layout resource id
     */
    protected abstract
    @LayoutRes
    int getLayoutId();

    protected abstract ViewBinding getViewBinding();


    protected abstract String getLayoutName();

    /**
     * This method called after initialize view
     */
    protected abstract void setupView();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getViewBinding() != null) {
            setLayoutView(getViewBinding(), getLayoutId(), getLayoutName());
        } else if (getLayoutId() != NO_UI_ACTIVITY) {
            setLayoutView(getLayoutId(), getLayoutName());
        }
        mViewModel = getViewModel();
        setupView();
    }

    protected abstract int getStyleId();
}
