package com.shopgolrang.base.mvvm;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewbinding.ViewBinding;

import com.shopgolrang.base.BaseFragment;


public abstract class BaseMVVMFragment<V extends BaseViewModel> extends BaseFragment {
    protected V mViewModel;

    protected abstract V getViewModel();

    /**
     * @return layout resource id
     */
    protected abstract int getLayoutId();

    protected abstract ViewBinding getBindView();


    /**
     * This method called after initialize view
     */
    protected abstract void setupView();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = getViewModel();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (currView == null) {
            if (getBindView() != null) {
                setLayoutView(getBindView(), getLayoutId(), "");
            } else {
                setLayoutView(getLayoutId(), inflater, container, "");
            }
            setupView();
        }
        return currView;
    }


    /*public void addDisposable(Disposable disposable) {
        mViewModel.getCompositeDisposable().add(disposable);
    }*/

    protected void changeFragment(Fragment fragment) {
        changeFragment(fragment, true);
    }

    protected void changeFragment(Fragment fragment, Boolean addToBackStack) {
      /*  if (mContext instanceof SwitchFragment) {
            ((SwitchFragment) mContext).onSwitch(fragment, addToBackStack, "", true);
        }*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getViewLifecycleOwnerLiveData().removeObservers(this);

    }

    protected void back() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

}
