package com.shopgolrang.utils;


import androidx.fragment.app.Fragment;

public interface SwitchFragments {
    void onSwitch(Fragment newFragment);

}
